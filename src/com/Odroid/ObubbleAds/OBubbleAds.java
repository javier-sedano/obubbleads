package com.Odroid.ObubbleAds;

import android.os.Bundle;
import android.util.Log;

import com.Odroid.ObubbleCore.OBubble;
import com.admob.android.ads.AdListener;
import com.admob.android.ads.AdManager;
import com.admob.android.ads.AdView;

public class OBubbleAds extends OBubble {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    AdManager.setTestDevices(new String[] {
        AdManager.TEST_EMULATOR, // Android emulator
        //        "92C1B56F7C969AC6A37A2CCBED67F4E6", // My phone
        });
//    ((AdView)findViewById(R.id.ad)).setAdListener(
//        new AdListener() {
//          public void onFailedToReceiveAd ( AdView adView ) {
//            // this is what logs
//              Log.w ( "0BubbleAds", "AdMob failed to receive" );
//          }
//          public void onFailedToReceiveRefreshedAd ( AdView adView ) {
//              Log.w ( "0BubbleAds", "AdMob failed to refresh" );
//          }
//          public void onReceiveAd ( AdView adView ) {
//              Log.w ( "0BubbleAds", "AdMob received" );
//          }
//          public void onReceiveRefreshedAd ( AdView adView ) {
//              Log.w ( "0BubbleAds", "AdMob refreshed" );
//          }
//      }
//    );
    ((AdView)findViewById(R.id.ad)).requestFreshAd();
  }
}
