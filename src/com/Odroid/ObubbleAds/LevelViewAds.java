package com.Odroid.ObubbleAds;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.Odroid.ObubbleCore.LevelView;
import com.admob.android.ads.AdView;
import com.mobclix.android.sdk.MobclixMMABannerXLAdView;

public class LevelViewAds extends LevelView {

  private AdView _adView = null;
  private MobclixMMABannerXLAdView _mobclixView = null;

  public LevelViewAds(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  private boolean flag = false;
  private int _lastState = STATE_RUNNING;

  @Override
  protected void _doDraw() {
    super._doDraw();
    if (_adView == null) {
      Context c = getContext();
      if (c instanceof Activity)
      ;
      {
        _adView = ((AdView) ((Activity) c).findViewById(R.id.ad));
      }
    }
    if (_mobclixView == null) {
      Context c = getContext();
      if (c instanceof Activity)
      ;
      {
        _mobclixView = ((MobclixMMABannerXLAdView) ((Activity) c).findViewById(R.id.banner_adview));
      }
    }

    if (_adView != null) {
      if (super._state != _lastState) {
        if (_state == STATE_RUNNING) {
          // Entering to RUNNING
          if (_adView != null) _adView.setVisibility(View.GONE);
          if (_mobclixView != null) _mobclixView.setVisibility(View.GONE);
          // android.util.Log.d("0BubbleAds", "AdMob: GONE");
        }
        if (_lastState == STATE_RUNNING) {
          // Leaving RUNNING
          if (_adView != null) _adView.setVisibility(View.VISIBLE);
          if (_adView != null) _adView.requestFreshAd();
          if (_mobclixView != null) _mobclixView.setVisibility(View.VISIBLE);
          if (_mobclixView != null) _mobclixView.getAd();
          // android.util.Log.d("0BubbleAds", "AdMob: VISIBLE");
        }
        _lastState = _state;
      }
    }
  }
}
